#include <iostream>

#include "vtkPVStringFormatter.h"

int main()
{
    vtkPVStringFormatter::PushScope("VIEW", fmt::arg("variance", 10.0));
    std::cout << vtkPVStringFormatter::Format("{VIEW_variance}") << std::endl;
    std::cout << "Hello, World!" << std::endl;
    return 0;
}
